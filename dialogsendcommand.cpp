#include "dialogsendcommand.h"
#include "ui_dialogsendcommand.h"
#include "mainwindow.h"
#include "stdint.h"

DialogSendCommand::DialogSendCommand(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSendCommand)
{
    ui->setupUi(this);
}

DialogSendCommand::~DialogSendCommand()
{
    delete ui;
}

void DialogSendCommand::on_pushButton_clicked()
{
    uint16_t addr = ui->lineEdit_address->text().toInt();
    uint16_t cmd = ui->lineEdit_cmd->text().toInt();
    uint16_t cmdData = ui->lineEdit_cmdData->text().toInt();

    if(ui->radioButton_2400->isChecked()){
        addr |= 0x0100; // set flag for 2.4GHz network.
    }
    MainWindow::app()->sendRadioCommand(addr, cmd, cmdData);
}
