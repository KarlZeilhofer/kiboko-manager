#ifndef DIALOGGENERALSETTINGS_H
#define DIALOGGENERALSETTINGS_H

#include <QDialog>

// defines for the names of the SettingS:
#define SS_GS_PREFIX					"GeneralSettings"
#define SS_GS_PUBLISH_INTERVAL			"PublishInterval"
#define SS_GS_START_INTERVAL			"CountdownInterval"
#define SS_GS_SIGNAL_ON_TRIGGER			"SignalOnTrigger"
#define SS_GS_SIGNAL_ON_START			"SignalOnStart"
#define SS_GS_PUBLISH_DSQ				"PublishDSQ"
#define SS_GS_PUBLISH_ERRORS			"PublishErrors"
#define SS_GS_ASSIGN_ONLY_FORWARD		"AssignOnlyForward"
#define SS_GS_ASSIGN_TO_CURRENT_ROUND	"AssignToCurrentRound"

namespace Ui {
    class DialogGeneralSettings;
}

class DialogGeneralSettings : public QDialog
{
    Q_OBJECT

public:
    explicit DialogGeneralSettings(QWidget *parent = 0);
    ~DialogGeneralSettings();
	
public slots:
	void accept();
	void reject();

private:
	void load();
	void save();
	
    Ui::DialogGeneralSettings *ui;
	int myPublishInterval;
};

#endif // DIALOGGENERALSETTINGS_H
