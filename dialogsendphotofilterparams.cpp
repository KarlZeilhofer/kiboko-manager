#include "dialogsendphotofilterparams.h"
#include "ui_dialogsendphotofilterparams.h"
#include "mainwindow.h"
#include "infoscreen.h"
#include "boxstates.h"
#include "packet.h"
#include <QDesktopServices>
#include <QUrl>

DialogSendPhotoFilterParams::DialogSendPhotoFilterParams(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::DialogSendPhotoFilterParams)
{
	ui->setupUi(this);

    boatboxID=1;

    synchronousPulseDetection=DEFAULT_SYNC_PULSE_MASK_JITTER_TOL;
    pulseMemoryLength=DEFAULT_PULSE_MEMORY_LENGTH;
    pulseDetectionLimit=DEFAULT_PULSE_DETECTION_LIMIT;
    missingPulseTolerance=DEFAULT_MISSING_PULSE_TOLERANCE;
    pulseJitterTolerance=DEFAULT_PULSE_JITTER_TOLERANCE;

	
	for(int i=0; i<N_BOATBOXES; i++)
	{
		ui->comboBox_boatboxID->addItem(QString::number(i+1));
	}

    connect(&responseUpdateTimer, SIGNAL(timeout()), this, SLOT(updateResponseValues()));

    responseUpdateTimer.setInterval(200);
    responseUpdateTimer.start();
	
	resetUI();
}

DialogSendPhotoFilterParams::~DialogSendPhotoFilterParams()
{
	delete ui;
}

void DialogSendPhotoFilterParams::on_buttonBox_accepted()
{
    on_pushButton_send_clicked();
}


void DialogSendPhotoFilterParams::on_buttonBox_rejected()
{
	resetUI();
}


void DialogSendPhotoFilterParams::on_pushButton_defaultSettings_clicked()
{
    synchronousPulseDetection=DEFAULT_SYNC_PULSE_MASK_JITTER_TOL;
    pulseMemoryLength=DEFAULT_PULSE_MEMORY_LENGTH;
	pulseDetectionLimit=DEFAULT_PULSE_DETECTION_LIMIT;
	missingPulseTolerance=DEFAULT_MISSING_PULSE_TOLERANCE;
	pulseJitterTolerance=DEFAULT_PULSE_JITTER_TOLERANCE;
	resetUI();
}


void DialogSendPhotoFilterParams::resetUI()
{
	// reset all widgets to their previous values
	ui->comboBox_boatboxID->setCurrentIndex(boatboxID-1);
    ui->spinBox_synchronousPulseDetection->setValue(synchronousPulseDetection);
    ui->spinBox_pulseMemoryLength->setValue(pulseMemoryLength);
    ui->spinBox_pulseDetectionLimit->setValue(pulseDetectionLimit);
	ui->spinBox_missingPulseTolerance->setValue(missingPulseTolerance);
    ui->spinBox_pulseJitterTolerance->setValue(pulseJitterTolerance);
}

void DialogSendPhotoFilterParams::updateResponseValues()
{
    boatboxID=ui->comboBox_boatboxID->currentIndex()+1;
    BoxStates::BoatBoxState::Response r;

    r = MainWindow::app()->boxStates.boatBoxState[boatboxID].responses[RResp_STATUS_SYNCHRONOUS_PULSE_MASK];
    if(r.lastTimeReceived.elapsed() > 10000)
        ui->lineEdit_synchronousPulseDetection->setText(QString("%1 ?").arg(r.data));
    else
        ui->lineEdit_synchronousPulseDetection->setText(QString("%1").arg(r.data));

    r = MainWindow::app()->boxStates.boatBoxState[boatboxID].responses[RResp_PULSE_MEMORY_LENGTH];
    if(r.lastTimeReceived.elapsed() > 10000)
        ui->lineEdit_pulseMemoryLength->setText(QString("%1 ?").arg(r.data));
    else
        ui->lineEdit_pulseMemoryLength->setText(QString("%1").arg(r.data));

    r = MainWindow::app()->boxStates.boatBoxState[boatboxID].responses[RResp_PULSE_DETECTION_LIMIT];
    if(r.lastTimeReceived.elapsed() > 10000)
        ui->lineEdit_pulseDetectionLimit->setText(QString("%1 ?").arg(r.data));
    else
        ui->lineEdit_pulseDetectionLimit->setText(QString("%1").arg(r.data));

    r = MainWindow::app()->boxStates.boatBoxState[boatboxID].responses[RResp_MISSING_PULSE_DETECTION_TOLERANCE];
    if(r.lastTimeReceived.elapsed() > 10000)
        ui->lineEdit_missingPulseTolerance->setText(QString("%1 ?").arg(r.data));
    else
        ui->lineEdit_missingPulseTolerance->setText(QString("%1").arg(r.data));

    r = MainWindow::app()->boxStates.boatBoxState[boatboxID].responses[RResp_PULSE_JITTER_TOLERANCE];
    if(r.lastTimeReceived.elapsed() > 10000)
        ui->lineEdit_pulseJitterTolerance->setText(QString("%1 ?").arg(r.data));
    else
        ui->lineEdit_pulseJitterTolerance->setText(QString("%1").arg(r.data));


    r = MainWindow::app()->boxStates.boatBoxState[boatboxID].responses[RResp_PULSE_SETTINGS_SAVED];
    if(r.lastTimeReceived.elapsed() < 10000)
    {
        if(r.data){ // if is saved
            ui->pushButton_save->setEnabled(false);
            ui->pushButton_restore->setEnabled(false);
        }else{
            ui->pushButton_save->setEnabled(true);
            ui->pushButton_restore->setEnabled(true);
        }
    }
}

void DialogSendPhotoFilterParams::on_pushButton_send_clicked()
{
    // read all values from widgets
    boatboxID=ui->comboBox_boatboxID->currentIndex()+1;

    synchronousPulseDetection=ui->spinBox_synchronousPulseDetection->value();
    pulseMemoryLength=ui->spinBox_pulseMemoryLength->value();
    pulseDetectionLimit=ui->spinBox_pulseDetectionLimit->value();
    missingPulseTolerance=ui->spinBox_missingPulseTolerance->value();
    pulseJitterTolerance=ui->spinBox_pulseJitterTolerance->value();

    // send to boatboxes
    emit sendRadioCommand((1<<8) | (boatboxID+1), RCmd_ENABLE_SYNCHRONOUS_PULSE_MASK, synchronousPulseDetection);
    emit sendRadioCommand((1<<8) | (boatboxID+1), RCmd_SET_PULSE_MEMORY_LENGTH, pulseMemoryLength);
    emit sendRadioCommand((1<<8) | (boatboxID+1), RCmd_SET_PULSE_DETECTION_LIMIT, pulseDetectionLimit);
    emit sendRadioCommand((1<<8) | (boatboxID+1), RCmd_SET_MISSING_PULSE_TOLERANCE, missingPulseTolerance);
    emit sendRadioCommand((1<<8) | (boatboxID+1), RCmd_SET_PULSE_JITTER_TOLERANCE, pulseJitterTolerance);

    MainWindow::app()->infoscreen()->appendInfo(tr("Einstellungen für Photodetektor gesendet"));

}

void DialogSendPhotoFilterParams::on_pushButton_save_clicked()
{
    boatboxID=ui->comboBox_boatboxID->currentIndex()+1;
    emit sendRadioCommand((1<<8) | (boatboxID+1), RCmd_SAVE_PULSE_SETTINGS, 0);

    ui->pushButton_save->setEnabled(false);
}

void DialogSendPhotoFilterParams::on_pushButton_restore_clicked()
{
    boatboxID=ui->comboBox_boatboxID->currentIndex()+1;
    emit sendRadioCommand((1<<8) | (boatboxID+1), RCmd_LOAD_PULSE_SETTINGS, 0);

    ui->pushButton_restore->setEnabled(false);
}

void DialogSendPhotoFilterParams::on_pushButton_help_clicked()
{
    QDesktopServices desk;
    desk.openUrl(QUrl("https://gitlab.com/KarlZeilhofer/kiboko-manager/wikis/photo-filter"));
}
